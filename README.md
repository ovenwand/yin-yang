# Yin Yang

## About the project
***# TODO***

## Development

### Setting up the environment
It is preferred to develop in a UNIX based operating system. If you're having trouble settings up consider using a VM to run your project.

I recommend using NVM to manage your node installation, NVM allows you to manage your node versions without touching the system node. Install instructions are found [here](https://github.com/creationix/nvm).

**Follow the following instructions to prepare your system for running the project:**
1. Make sure you have [git](https://git-scm.com/) installed
2. (optional) Install NVM using the provided instructions
3. Install the [yarn](https://yarnpkg.com/) package globally: `npm i -g yarn`

### Setting up the project
1. Create a folder to hold the project files and move into the newly created folder
2. Clone the latest version of the repo into your new project folder: `git clone git@gitlab.com:ovenwand/yin-yang.git`
3. Install the project dependencies: `yarn`

Your project should now be set up

### Running the game
To run the game during development simply run the following command: `yarn start`. This will start a [Webpack Dev Server](https://github.com/webpack/webpack-dev-server) to serve all our bundle files, which then launches [Electron](https://electronjs.org). WDS will watch files changes and rebuild after every file change. WDS will try to use [HMR](https://webpack.js.org/concepts/hot-module-replacement/) to update the page without reloading the page, otherwise it'll refresh the page on every rebuild.

### Version control
The project uses git for version control

#### Branches
* `master` The main branch, this branch contains the latest releasable version of the game. It should not contain any bugs. Maintainers can push/merge to this branch.
* `staging` The development branch, any fixed bugs, newly developed features, refactors, etc will be merged into this branch. Maintainers can push/merge to this branch.
* `<feature>` Feature branches can be named whatever you like. Feature branches may contain any kind of changes, you are free to do whatever you like. Anyone can push/merge to these branches.

#### Workflow
Development happens on `<feature>` branches. Generally speaking it's recommended to create all new `<feature>` branches from the latest `staging` branch. Commit your changes on the `<feature>` branch. Once you're satisfied with the changes you made and tested them thoroughly, you're ready to merge into the `staging` branch. To do this you have to create a [merge request](https://gitlab.com/ovenwand/yin-yang/merge_requests/new) in Gitlab with `staging` as the target branch. Other developers and mainatainers will be able to run and test the latest staging version. Once satisfied it will be merge into master and a new release will be published. 

## Deployment
***# TODO***
