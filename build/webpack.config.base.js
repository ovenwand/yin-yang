const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { isProduction, resolve } = require('./utils');

module.exports = {
    cache: !isProduction,

    output: {
        path: resolve('dist'),
        filename: '[name].js',
        publicPath: '/',
    },

    stats: {
        colors: true,
        children: false,
        modules: false,
        chunks: false,
    },

    resolve: {
        extensions: ['.ts', '.js', '.json'],
        modules: [resolve('node_modules'), resolve('src')],
    },

    plugins: [
        new webpack.NamedModulesPlugin(),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'src/index.html',
            inject: true,
        }),
    ],

    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'tslint-loader',
                enforce: 'pre',
                exclude: /node_modules/,
            },
            {
                test: /\.ts?$/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            cacheDirectory: true,
                        },
                    },
                    'ts-loader',
                    'tslint-loader',
                ],
                exclude: /node_modules/,
            },
        ],
    },
};
