const webpack = require('webpack');
const merge = require('webpack-merge');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const baseWebpackConfig = require('./webpack.config.base');
const { resolve } = require('./utils');

const analyzerMode = process.env.WEBPACK_ANALYZE ? 'server' : 'disabled';

const prodWebpackConfig = merge.smart(baseWebpackConfig, {
    mode: 'production',

    devtool: 'source-map',

    optimization: {
        minimize: true,
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /node_modules/,
                    chunks: 'all',
                    name: 'vendor',
                },
            },
        },
    },

    plugins: [
        new webpack.DefinePlugin({ 'process.env': require('../config/prod.env') }),
        new CleanWebpackPlugin([resolve('dist/*')], { root: resolve() }),
        new CopyWebpackPlugin([{
            from: resolve('static'),
            to: resolve('dist'),
            ignore: ['.*'],
        }]),
        new BundleAnalyzerPlugin({ analyzerMode }),
    ],
});

module.exports = [
  merge.smart(prodWebpackConfig, {
    entry: { 'main.web': './src/main.ts' },
  }),
    merge.smart(prodWebpackConfig, {
        target: 'electron-main',
        entry: { main: './src/main.ts' },
    }),
    merge.smart(prodWebpackConfig, {
        target: 'electron-renderer',
        entry: { main: './src/renderer.ts' },
    }),
];
