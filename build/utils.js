const path = require('path');

const isProduction = process.env.NODE_ENV === 'production';
const isDevelopment = process.env.NODE_ENV === 'development';

function resolve(...args) {
    return path.resolve(__dirname, '..', ...args);
}

module.exports = {
    isProduction,
    isDevelopment,
    resolve,
};
