const portfinder = require('portfinder');
const webpack = require('webpack');
const merge = require('webpack-merge');
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');
const { spawn } = require('child_process');
const packageConfig = require('../package');
const baseWebpackConfig = require('./webpack.config.base');
const { resolve } = require('./utils');

const devWebpackConfig = merge.smart(baseWebpackConfig, {
  mode: 'development',

  devServer: {
    contentBase: resolve('static'),
    historyApiFallback: true,
    hot: true,
    compress: true,
    open: false,
    overlay: { warnings: false, errors: true },
    publicPath: 'http://www.ovenwand.local:1337/',
    quiet: true,
    host: 'www.ovenwand.local',
    port: 1337,
    before() {
      spawn('electron', [devWebpackConfig.devServer.publicPath], { shell: true, env: process.env, stdio: 'inherit' })
        .on('close', () => process.exit(0))
        .on('error', (e) => console.error(e));
    },
  },

  devtool: 'eval-source-map',

  plugins: [
    new webpack.DefinePlugin('process.env', require('../config/dev.env')),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
  ],
});

function createNotifierCallback() {
  const notifier = require('node-notifier');

  return (severity, errors) => {
    if (severity !== 'error') {
      return;
    }
    const error = errors[0];
    const filename = error.file && error.file.split('!').pop();

    notifier.notify({
      title: packageConfig.name,
      message: `${severity}: ${error.name}`,
      subtitle: filename || '',
      icon: resolve('static/icon.png'),
    });
  };
}

module.exports = new Promise((resolve, reject) => {
  portfinder.basePort = process.env.PORT || devWebpackConfig.devServer.port;

  portfinder.getPort((err, port) => {
    if (err) {
      reject(err);
    } else {
      devWebpackConfig.plugins.push(new FriendlyErrorsPlugin({
        compilationSuccessInfo: {
          messages: [`Your application is running at: http://${devWebpackConfig.devServer.host}:${port}`],
        },
        onErrors: createNotifierCallback(),
      }));
    }
  });

  resolve([
    // merge.smart(devWebpackConfig, {
    //   entry: { 'main.web': './src/main.ts' },
    // }),
    merge.smart(devWebpackConfig, {
      target: 'electron-main',
      entry: { main: './src/main.ts' },
    }),
    merge.smart(devWebpackConfig, {
      target: 'electron-renderer',
      entry: { renderer: './src/renderer.ts' },
    }),
  ]);
});
