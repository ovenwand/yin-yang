import { app, BrowserWindow } from 'electron';

const isDevelopment = process.env.NODE_ENV === 'development';

let mainWindow: BrowserWindow | null;

function createWindow(): BrowserWindow {
  const window = new BrowserWindow({ height: 667, width: 913 });

  if (isDevelopment) {
    window.webContents.openDevTools();
    window.loadURL('http://www.ovenwand.local:1337/index.html');
  } else {
    window.loadFile('dist/index.html');
  }

  return window;
}

app.on('window-all-closed', () => {
  console.log('App window-all-closed');

  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  console.log('App activate');

  if (mainWindow === null) {
    mainWindow = createWindow();
  }
});

app.on('ready', () => {
  console.log('App ready');

  if (isDevelopment) {
    require('vue-devtools').install();
  }

  mainWindow = createWindow();
});
