/**
 * Config
 */
declare interface GameConfig {
  controls: ControlsConfig;
  graphics: GraphicsConfig;
}

declare interface GraphicsConfig {
  fps: number;
}

declare interface ControlsConfig {
  menu: {
    down: KeyboardControlKeys,
    select: KeyboardControlKeys,
    up: KeyboardControlKeys,
  },
  game: {
    menu: KeyboardControlKeys,
    playerMoveLeft: KeyboardControlKeys,
    playerMoveRight: KeyboardControlKeys,
    playerJump: KeyboardControlKeys,
    playerCrouch: KeyboardControlKeys,
  },
}

/**
 * Keyboard
 */
declare type KeyboardControlKeys = string[];
declare type KeyboardHandler = () => void;

declare interface KeyboardHandlers {
  press?: KeyboardHandler;
  release?: KeyboardHandler;
}


/**
 * Renderable
 */
declare type RenderableRenderFunction = () => PIXI.Graphics;

/**
 * GameObject
 */
declare type GameObjectId = symbol;

declare interface GameObjectPosition {
  bottom: number;
  left: number;
  right: number;
  top: number;
}

declare interface GameObjectOptions  {
  name?: string;
  rigid?: boolean;
  render: RenderableRenderFunction;
}

declare interface GameObjectManagerOptions {
  collisions: boolean;
}


/**
 * Mob
 */
declare interface MobOptions extends GameObjectOptions {
  hostility?: number;
  gravity?: number;
  speed?: number;
}

/**
 * Collision
 */
declare type CollisionSide = 'bottom' | 'left' | 'right' | 'top' | 'any';

declare interface Collision {
    pos: GameObjectPosition;
    bottom: boolean;
    left: boolean;
    right: boolean;
    top: boolean;
}

declare interface Collisions {
  [key: string]: Collision;
}


/**
 * Player
 */
declare interface PlayerOptions {
  color: number;
  controls: PlayerControls;
}

declare interface PlayerControls {
  Crouch: KeyboardControlKeys;
  Jump: KeyboardControlKeys;
  MoveLeft: KeyboardControlKeys;
  MoveRight: KeyboardControlKeys;
}


/**
 * Map
 */
declare type MapTileType = number;

declare interface MapBlueprint {
  map: {
    light: MapTileType[][],
    dark: MapTileType[][],
  };
  tiles: { [type: number]: MapTile };
}
