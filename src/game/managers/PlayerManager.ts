import Player from '../objects/Player';
import MobManager from './MobManager';

export default class PlayerManager extends MobManager {
  protected objects: Map<symbol, Player>;

  constructor(players: Player[] = []) {
    super(players);
  }
}
