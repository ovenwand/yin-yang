import GameObjectManager from './GameObjectManager';
import Floor from '../objects/Floor';

export default class FloorManager extends GameObjectManager {
  protected objects: Map<symbol, Floor>;

  constructor(floors: Floor[] = []) {
    super(floors);
  }
}
