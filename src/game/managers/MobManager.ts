import GameObjectManager from './GameObjectManager';
import Mob from '../objects/Mob';

export default class MobManager extends GameObjectManager {
  protected objects: Map<symbol, Mob>;

  constructor(mobs: Mob[]) {
    super(mobs);
  }

  public state(delta: number) {
    super.state(delta);

    this.objects.forEach((obj) => {
      obj.state(delta);
    });
  }
}
