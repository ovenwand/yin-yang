import EventBus from '../core/EventBus';
import GameObject from '../core/GameObject';

export default class GameObjectManager {
  protected objects: Map<GameObjectId, GameObject>;
  protected options: GameObjectManagerOptions;

  public event: EventBus;

  constructor(objects: GameObject[] = [], options: GameObjectManagerOptions = { collisions: false }) {
    this.options = options;
    this.objects = new Map();
    this.event = new EventBus();

    for (const obj of objects) {
      this.objects.set(obj.id, obj);
    }
  }

  // Each subclass of the GameObject class should handle their own state updates.
  // Since collisions have to be handled in the GameObject class we still need
  // this state update in our game.objects manager.
  public state(delta: number) {
    this.objects.forEach((obj) => {
      if (this.options.collisions) {
        this.objects.forEach((compare) => {
          if (obj !== compare) {
            obj.collisions.check(compare);
          }
        });
      }
    });
  }

  public add(...objects: GameObject[]) {
    this.event.emit('add', objects);

    for (const obj of objects) {
      if (!this.objects.has(obj.id)) {
        this.objects.set(obj.id, obj);
      }
    }
  }

  public remove(...objects: GameObject[]) {
    this.event.emit('remove', objects);

    for (const obj of objects) {
      if (this.objects.has(obj.id)) {
        this.objects.delete(obj.id);
      }
    }
  }

  public spawn(stage: PIXI.Container) {
    this.objects.forEach((obj) => {
      obj.spawn(stage);
    });
  }

  public despawn(stage?: PIXI.Container) {
    this.objects.forEach((obj) => {
      obj.despawn(stage);
    });
  }
}
