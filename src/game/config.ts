const graphics: GraphicsConfig = {
  fps: 60,
};

const controls: ControlsConfig = {
  game: {
    menu: ['Escape', 'F12'],
    playerCrouch: ['ArrowDown', 'Control', 's'],
    playerJump: ['ArrowUp', ' ', 'w'],
    playerMoveLeft: ['ArrowLeft', 'a'],
    playerMoveRight: ['ArrowRight', 'd'],
  },
  menu: {
    down: ['ArrowDown', 's'],
    select: ['Enter'],
    up: ['ArrowUp', 'w'],
  },
};

export default { controls, graphics } as GameConfig;
