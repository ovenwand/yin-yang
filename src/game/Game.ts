import * as PIXI from 'pixi.js';
import config from './config';
import GameObjectManager from './managers/GameObjectManager';
import GameViewManager from './managers/GameViewManager';
import PlayerManager from './managers/PlayerManager';
import Floor from './objects/Floor';
import Player from './objects/Player';
import FloorManager from './managers/FloorManager';

export default class Game {
  public static MIN_HEIGHT = 300;
  public static MIN_WIDTH = 300;

  public app: PIXI.Application;
  public root: HTMLElement;
  public view: HTMLCanvasElement;
  public views: GameViewManager;
  public objects: GameObjectManager;
  public players: PlayerManager;
  public floors: FloorManager;

  constructor(options: PIXI.ApplicationOptions = { height: 720, width: 1280 }) {
    this.app =  new PIXI.Application(options);
    this.view = this.app.view;
    this.loop = this.loop.bind(this);

    // Important stuff XD
    this.app.renderer.autoResize = true;
    this.app.ticker.speed = config.graphics.fps / this.app.ticker.FPS;
  }

  public initialize() {
    if (this.view.parentNode && this.view.parentNode instanceof HTMLElement) {
      this.root = this.view.parentNode;
      this.objects = new GameObjectManager([], { collisions: true });
      this.floors = new FloorManager();
      this.players = new PlayerManager();

      // Make sure all objects are also added to the main GameObjectManager.
      // This should probably be refactored so we don't need to manually register
      // other managers to the main manager in `this.objects`
      this.floors.event.on('add', (f) => this.objects.add(...f));
      this.floors.event.on('remove', (f) => this.objects.remove(...f));
      this.players.event.on('add', (p) => this.objects.add(...p));
      this.players.event.on('remove', (p) => this.objects.remove(...p));

      window.addEventListener('resize', () => {
        this.resize(this.root.offsetWidth, this.root.offsetHeight);
      });

      this.resize(this.root.offsetWidth, this.root.offsetHeight);
    }
  }

  public launch() {
    this.initialize();
    this.start();
  }

  public start() {
    this.floors.add(new Floor(
      0x00FF00,
      window.innerWidth * 1.2,
      window.innerHeight * .2,
      -window.innerWidth * .1,
    ));

    this.players.add(new Player('Yin', {
      color: 0xFF0000,
      controls: {
        Crouch: ['s'],
        Jump: ['w'],
        MoveLeft: ['a'],
        MoveRight: ['d'],
      },
    }));

    this.players.add(new Player('Yang', {
      color: 0x0000FF,
      controls: {
        Crouch: ['ArrowDown'],
        Jump: ['ArrowUp'],
        MoveLeft: ['ArrowLeft'],
        MoveRight: ['ArrowRight'],
      },
    }));

    this.floors.spawn(this.app.stage);
    this.players.spawn(this.app.stage);

    this.app.ticker.start();
    this.app.ticker.add(this.loop);
  }

  public pause() {
    this.app.ticker.stop();
  }

  public continue() {
    this.app.ticker.start();
  }

  public stop() {
    this.app.ticker.stop();
    this.floors.despawn();
    this.players.despawn();
  }

  public resize(width: number, height: number) {
    if (width >= Game.MIN_WIDTH && height >= Game.MIN_HEIGHT) {
      this.app.renderer.resize(width, height);
    }
  }

  private loop(delta: number) {
    this.state(delta);

    this.objects.state(delta);
    this.floors.state(delta);
    this.players.state(delta);
  }

  private state(delta: number) {
    // noop
  }
}

