import GameObject from '../core/GameObject';

const HOSTILITY = {
  FRIENDLY: 1,
  HOSTILE: 2,
  NEUTRAL: 0,
};

export default class Mob extends GameObject {
  public static DEFAULT_HOSTILITY = HOSTILITY.NEUTRAL;
  public static DEFAULT_GRAVITY = 1.6;
  public static DEFAULT_SPEED = 1;
  public static INITIAL_FALL_VELOCITY = 4;
  public static INITIAL_JUMP_VELOCITY = 25;

  private fallVelocity: number;
  private jumpVelocity: number;
  private crouchHeight: number;
  private originalHeight: number;

  private _gravity: number;
  public speed: number;
  public hostility: number;

  // I think the getter/setter set up for this.gravity is ugly,
  // there should be a better way to fix this right? I don't
  // like the this._gravity
  public get gravity() {
    return this._gravity / 10;
  }

  public set gravity(gravity) {
    this._gravity = gravity;
  }

  public falling: boolean;
  public shouldJump: boolean;
  public jumping: boolean;
  public crouching: boolean;

  constructor(options: MobOptions) {
    super({ name: options.name, render: options.render });
    this.hostility = options.hostility || Mob.DEFAULT_HOSTILITY;
    this.speed = options.speed || Mob.DEFAULT_SPEED;
    this.gravity = options.gravity || Mob.DEFAULT_GRAVITY;

    this.jumpVelocity = Mob.INITIAL_JUMP_VELOCITY;
    this.fallVelocity = Mob.INITIAL_FALL_VELOCITY;
    this.crouchHeight = this.height * (2 / 3);
    this.originalHeight = this.height;
  }

  public state(delta: number) {
    const bottomCollision = this.collisions.has('bottom');

    this.move(delta);

    if (bottomCollision) {
      this.land(bottomCollision.top);
    } else if (!this.jumping) {
      this.falling = true;
    }

    if (this.falling) {
      this.fall(delta);
    } else if (this.shouldJump || this.jumping) {
       // I can't remember why I needed this,
      // but it breaks when I remove it....
      this.jumping = true;
      this.jump(delta);
    }

    if (this.crouching) {
      this.crouch();
    } else {
      this.stand();
    }
  }

  protected move(delta: number) {
    this.vx *= this.speed;
    this.vy *= this.speed;
    super.move(delta);
  }

  public land(finalPosition: number) {
    this.fallVelocity = Mob.INITIAL_FALL_VELOCITY;
    this.falling = false;
    this.vy = 0;
    this.y = finalPosition - this.height;
  }

  public jump(delta: number) {
    this.jumpVelocity -= (this.jumpVelocity * this.gravity);

    if (Math.round(this.jumpVelocity) >= 1) {
      this.vy = -this.jumpVelocity * delta;
    } else {
      // this.fallVelocity = this.jumpVelocity + (this.jumpVelocity * this.gravity);
      this.fallVelocity = this.jumpVelocity;
      this.jumpVelocity = Mob.INITIAL_JUMP_VELOCITY;
      this.jumping = false;
      this.falling = true;
    }
  }

  public fall(delta: number) {
    this.fallVelocity += this.fallVelocity * this.gravity;
    this.vy = this.fallVelocity * delta;
  }

  public crouch() {
    if (this.height === this.originalHeight) {
      this.y += this.originalHeight - this.crouchHeight;
      this.height = this.crouchHeight;
    }
  }

  public stand() {
    if (this.height !== this.originalHeight) {
      this.y -= (this.originalHeight - this.crouchHeight);
      this.height = this.originalHeight;
    }
  }

  public shouldCollide(compare: GameObject): boolean {
    return !(compare instanceof Mob);
  }
}
