import KeyboardControl from '../core/KeyboardControl';
import KeyboardControls from '../core/KeyboardControls';
import Mob from './Mob';

export default class Player extends Mob {
  public static readonly ABSOLUTE_SPEED = 8;

  public static render(color: number, scale: number = 1) {
    const height = 150 * scale;
    const width = 85 * scale;

    const graphics = new PIXI.Graphics()
      .lineStyle(2, color, 1)
      .beginFill(color, 0.4)
      .drawRect(0, 0, width, height);

    graphics.height = height;
    graphics.width = width;

    return graphics;
  }

  private controls: KeyboardControls;
  private movingLeft: boolean;
  private movingRight: boolean;

  constructor(name: string, options: PlayerOptions) {
    const render = () => Player.render(options.color);
    super({ name, render });

    this.controls = new KeyboardControls([
      new KeyboardControl(options.controls.Crouch, {
        press: () => this.crouching = true,
        release: () => this.crouching = false,
      }),
      new KeyboardControl(options.controls.Jump, {
        press: () => this.shouldJump = true,
        release: () => this.shouldJump = false,
      }),
      new KeyboardControl(options.controls.MoveLeft, {
        press: () => this.movingLeft = true,
        release: () => this.movingLeft = false,
      }),
      new KeyboardControl(options.controls.MoveRight, {
        press: () => this.movingRight = true,
        release: () => this.movingRight = false,
      }),
    ]);
  }

  public state(delta: number) {
    if (this.movingLeft) {
      this.vx = -Player.ABSOLUTE_SPEED;
    } else if (this.movingRight) {
      this.vx = Player.ABSOLUTE_SPEED;
    } else {
      this.vx = 0;
    }

    super.state(delta);
  }

  public spawn(stage: PIXI.Container) {
    this.controls.enable();
    return super.spawn(stage);
  }

  public despawn(stage: PIXI.Container) {
    this.controls.disable();
    return super.despawn(stage);
  }
}
