import GameObject from '../core/GameObject';

export default class Floor extends GameObject {
  public static render(
    color: number,
    width: number = 50,
    height: number = 1000,
    x: number = 0,
    y: number = 0,
  ) {
    const lineWidth = height * .2;
    y = y + window.innerHeight - height + lineWidth;

    const graphics = new PIXI.Graphics()
      .lineStyle(lineWidth, color, 1)
      .beginFill(color, 0.8)
      .drawRect(0, 0, width, height);

    graphics.x = x;
    graphics.y = y;
    graphics.height = height + (lineWidth * 2);
    graphics.width = width + (lineWidth * 2);

    return graphics
  }

  public state(delta: number) {
    // noop
  }

  constructor(color: number, width?: number, height?: number, x?: number, y?: number) {
    const render = () => Floor.render(color, width, height, x, y);
    super({ render });
  }

  public spawn(stage: PIXI.Container) {
    return super.spawn(stage);
  }
}
