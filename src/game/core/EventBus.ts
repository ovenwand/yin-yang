declare type EventHandler = (payload: any) => void;

export default class EventBus {
  private readonly events: { [name: string]: EventHandler[] };

  constructor() {
    this.events = Object.create(null);
  }

  public emit(name: string, payload: any) {
    if (this.events[name]) {
      this.events[name].forEach((handler) => handler(payload));
    }
  }

  public on(name: string, handler: EventHandler) {
    this.events[name] = this.events[name] || [];
    this.events[name].push(handler);
  }

  public once(name: string, handler: EventHandler) {
    this.events[name] = this.events[name] || [];
    handler = (payload) => {
      handler(payload);
      this.off(name, handler);
    };

    this.events[name].push(handler);
  }

  public off(name: string, handler?: EventHandler) {
    if (!handler) {
      delete this.events[name];
    } else {
      if (this.events[name]) {
        const index = this.events[name].indexOf(handler);
        this.events[name].splice(index, 1);
      }
    }
  }
}
