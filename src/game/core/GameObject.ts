import uuid from '../../utils/uuid';
import Collisions from './Collisions';
import Renderable from './Renderable';

export default class GameObject extends Renderable {
  private readonly _id: string;

  public readonly id: GameObjectId;
  public readonly name: string;
  public readonly type: typeof GameObject;
  public graphics: PIXI.Graphics;
  public collisions: Collisions;

  public rigid: boolean;
  public vx: number;
  public vy: number;

  constructor(options: GameObjectOptions) {
    super(options.render);

    this._id = uuid();
    this.id = Symbol(this._id);
    this.name = options.name || this._id;
    this.type = Object.getPrototypeOf(this); // Why can't this be this.constructor? Isn't it the same thing?

    this.collisions = new Collisions(this);

    this.rigid = options.rigid || true;
    this.vx = 0;
    this.vy = 0;
  }

  public state(delta: number) {
    this.move(delta);
  }

  public spawn(stage: PIXI.Container) {
    if (stage.children.includes(this.graphics)) {
      this.graphics.visible = true;
    } else {
      stage.addChild(this.graphics);
    }
  }

  public despawn(stage?: PIXI.Container) {
    if (stage) {
      stage.removeChild(this.graphics);
    } else {
      this.graphics.visible = false;
    }
  }

  protected move(delta: number) {
    this.x += this.vx * delta;
    this.y += this.vy * delta;
  }

  public shouldCollide(compare: GameObject): boolean {
    return this.rigid && compare.rigid;
  }
}
