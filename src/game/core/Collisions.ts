import GameObject from './GameObject';

export default class Collisions {
  private collisions: Map<GameObjectId, Collision>;
  private object: GameObject;

  constructor(object: GameObject) {
    this.collisions = new Map();
    this.object = object;
  }

  public check(compare: GameObject) {
    if (!this.object.shouldCollide(compare)) {
      return false;
    }

    const aPos = this.object.position();
    const bPos = compare.position();

    // We want to know whether it is going to collide,
    // so we need to add the x/y velocities.
    aPos.bottom += this.object.vy;
    aPos.left += this.object.vx;
    aPos.right += this.object.vx;
    aPos.top += this.object.vy;

    // Check for each side whether it overlaps
    let bottom = aPos.bottom >= bPos.top && aPos.bottom <= bPos.bottom;
    let left = aPos.left <= bPos.right && aPos.left >= bPos.left;
    let right = aPos.right >= bPos.left && aPos.right <= bPos.right;
    let top = aPos.top <= bPos.bottom && aPos.top >= bPos.top;

    // Check for inbounds collisions
    bottom = (left || right) && bottom;
    left = (bottom || top) && left;
    right = (bottom || top) && right;
    top =  (left || right) && top;

    this.collisions.set(compare.id, { pos: bPos, bottom, left, right, top });

    return bottom || left || right || top;
  }

  public has(side?: CollisionSide, id?: GameObjectId) {
    let collision: GameObjectPosition | false = false;

    if (id) {
      const collisions = this.collisions.get(id);

      if (collisions) {
        collision = this.findCollision(collisions, side);
      }
    } else {
      for (const key of this.collisions.keys()) {
        const collisions = this.collisions.get(key);

        if (collisions) {
          collision = this.findCollision(collisions, side);
          break;
        }
      }
    }

    return collision;
  }

  // Should this be a static or local method?
  private findCollision(collisions: Collision, side?: CollisionSide) {
    if (side && side !== 'any') {
      if (collisions[side]) {
        return collisions.pos;
      }
    } else {
      if (Object.values(collisions).includes(true)) {
        return collisions.pos;
      }
    }

    return false;
  };
}
