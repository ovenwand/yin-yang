
export default class Renderable {
  public graphics: PIXI.Graphics;

  public get x() {
    return this.graphics.x;
  }

  public set x(x) {
    this.graphics.x = x;
  }

  public get y() {
    return this.graphics.y;
  }

  public set y(y) {
    this.graphics.y = y;
  }

  public get height() {
    return this.graphics.height;
  }

  public set height(height) {
    this.graphics.height = height;
  }

  public get width() {
    return this.graphics.width;
  }

  public set width(width) {
    this.graphics.width = width;
  }

  constructor(render: RenderableRenderFunction) {
    this.graphics = render();
  }

  public position(): GameObjectPosition {
    return {
      bottom: this.y + this.height,
      left: this.x,
      right: this.x + this.width,
      top: this.y,
    };
  }
}
