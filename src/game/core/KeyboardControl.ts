export default class KeyboardControl {
  private readonly keys: KeyboardControlKeys;
  public press?: KeyboardHandler;
  public release?: KeyboardHandler;
  public isDown: boolean;
  public isUp: boolean;

  constructor(keys: KeyboardControlKeys, { press, release }: KeyboardHandlers) {
    this.keys = keys;
    this.press = press;
    this.release = release;
    this.onKeyDown = this.onKeyDown.bind(this);
    this.onKeyUp = this.onKeyUp.bind(this);
  }

  public bind() {
    window.addEventListener('keydown', this.onKeyDown, false);
    window.addEventListener('keyup', this.onKeyUp, false);
  }

  public unbind() {
    window.removeEventListener('keydown', this.onKeyDown, false);
    window.removeEventListener('keyup', this.onKeyUp, false);
  }

  private onKeyDown(event: KeyboardEvent) {
    this.handleEvent(event, () => {
      this.isDown = true;
      this.isUp = false;
      this.press && this.press();
    });
  }

  private onKeyUp(event: KeyboardEvent) {
    this.handleEvent(event, () => {
      this.isDown = false;
      this.isUp = true;
      this.release && this.release();
    });
  }

  private handleEvent(event: KeyboardEvent, fn: () => void) {
    if (this.keys.includes(event.key)) {
      fn();
    }

    // event.preventDefault();
  }
}
