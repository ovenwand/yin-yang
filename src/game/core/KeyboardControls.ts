import KeyboardControl from './KeyboardControl';

export default class KeyboardControls {
  private controls: KeyboardControl[];

  constructor(controls?: KeyboardControl[]) {
    this.controls = controls || [];
  }

  public add(control: KeyboardControl) {
    control.bind();
    this.controls.push(control);
  }

  public remove(control: KeyboardControl) {
    const index = this.controls.indexOf(control);

    control.unbind();

    if (~index) {
      this.controls.splice(index, 1);
    }
  }

  public enable() {
    for (const control of this.controls) {
      control.bind();
    }
  }

  public disable() {
    for (const control of this.controls) {
      control.unbind();
    }
  }
}
