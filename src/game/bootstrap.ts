import Game from './Game';

const root = document.getElementById('root') || document.body;
const game = new Game();

root.appendChild(game.view);

game.launch();
