export default function hasKeyboardSupport() {
  return 'KeyboardEvent' in window || 'onkeyup' in window;
}
